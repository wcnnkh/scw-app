package scw.app.payment.event;

import scw.event.BasicEventDispatcher;

public interface PaymentEventDispatcher extends BasicEventDispatcher<PaymentEvent> {
}
