package scw.app.logistics.event;

import scw.event.BasicEventDispatcher;

/**
 * 物流事件分发
 * @author shuchaowen
 *
 */
public interface LogisticsEventDispatcher extends BasicEventDispatcher<LogisticsEvent> {
}
