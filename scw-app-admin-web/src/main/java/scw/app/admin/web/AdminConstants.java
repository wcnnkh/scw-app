package scw.app.admin.web;

public final class AdminConstants {
	private AdminConstants(){};
	
	public static final String ADMIN_CONTROLLER_PREFIX = "${scw.admin.controller.prefix:/admin}";
	public static final String ROUTE_ATTR_NAME = "route";
	public static final String ICON_ATTR_NAME = "icon";
}
